# Adding MQTT support

## Add libraries for mqtt (mosquittopp library and headers)

sudo apt install libmosquittopp1 libmosquittopp-dev

## Add a local MQTT server & tools for it

sudo apt install mosquitto mosquitto-clients

## Testing installation of the MQTT server

### Subscribe to all messages from local broker and print them /w topic:
mosquitto_sub -v -t '#'
-v makes it print the topic. -t '#' subscribes to all topics

### Publish a message to local broker
mosquitto_pub -t /asdf -m huhu
-t is the topic, -m the message

# Accessing mqtt from C++

Docs here: https://mosquitto.org/api/files/cpp/mosquittopp-h.html
(this is just a C++ wrapper to methods shown here: https://mosquitto.org/api/files/mosquitto-h.html)

The normal way to access a broker is:
- initialize the lib
- connect to it: connect(address, port, timeout)
- start the main loop (_can_ be omitted if we dont plan to receive anything...)
- send something via publish()
- potentially receive something after subscribing to a topic
- call disconnect()
- stop the main loop (only if actually starting it...)
- library cleanup


# Systemd-Config

## Info about a package
dpkg-deb -I x.deb

## Install a package
sudo dpkg -i x.deb

## Auto-setting dependencies /w CPack:
SET(CPACK_DEBIAN_PACKAGE_SHLIBDEPS ON)
