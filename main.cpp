#include <chrono>
#include <iostream>
#include <string_view>
#include <thread>

#include <mosquittopp.h>

int main(int argc, char** argv)
{

    // note: depending on the compiler / lib version, the mosqpp:: calls might be marked as deprecated...

    mosqpp::lib_init();

    static const std::string serverAddress("127.0.0.1");
    constexpr uint16_t serverPort(1883u);
    constexpr uint32_t keepAliveTime(60u);

    // create the mosquitto connection...
    auto mqtt = mosqpp::mosquittopp();

    mqtt.connect(serverAddress.data(), serverPort, keepAliveTime);
    mqtt.loop_start();

    static const std::string topic("/temperature");
    static const std::string sourceFile("/sys/class/thermal/thermal_zone0/temp");
    std::cout << "Starting measurement of " << sourceFile << std::endl;

    while (true) {
        auto file = fopen(sourceFile.c_str(), "r");
        std::string currentTemperature;

        // get the data...
        auto c = getc(file);
        while (c != EOF) {
            currentTemperature += c;
            c = getc(file);
        }

        // put it to mqtt...
        auto temp = std::to_string(std::atof(currentTemperature.c_str()) / 1000.0);
        mqtt.publish(nullptr, topic.data(), temp.size(), temp.data());
        std::cout << "Temperature: " << temp << std::endl;
        fclose(file);

        // and wait five seconds...
        std::this_thread::sleep_for(std::chrono::seconds(5));
    }
    std::cout << "Quitting..." << std::endl;

    // force stopping
    mqtt.disconnect();
    mqtt.loop_stop();
    mosqpp::lib_cleanup();

    return 0;
}
