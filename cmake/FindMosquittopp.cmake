# if this is already set do not throw errors here... most likely this has been searched already _or_ the user provided this beforehand.
if(MOSQUITTOPP_INCLUDE_DIR)
    set(MOSQUITTOPP_FIND_QUIETLY TRUE)
endif()

# tell cmake to search the include file...
find_path(MOSQUITTOPP_INCLUDE_DIR mosquitto.h)

# ... and the libraries...
find_library(MOSQUITTOPP_LIBRARY NAMES libmosquittopp mosquittopp)

# use the default cmake-way to find the library and the args etc...
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(MOSQUITTOPP DEFAULT_MSG MOSQUITTOPP_LIBRARY MOSQUITTOPP_INCLUDE_DIR)

# now - if we found it - set the library path. Otherwise, set it to empty, causing the process to fail later on.
if(MOSQUITTOPP_FOUND)
  set(MOSQUITTOPP_LIBRARIES ${MOSQUITTOPP_LIBRARY})
else()
  set(MOSQUITTOPP_LIBRARIES)
endif()

# do not show he variable in the default view...
mark_as_advanced(MOSQUITTOPP_INCLUDE_DIR MOSQUITTOPP_LIBRARY)
