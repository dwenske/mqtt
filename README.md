# Mqtt-Demo - Training materials

## Step 1: Basic cmake build setup

- Contains only a _minimal_ setup for a CMake-based C++-application: Hello, world...
- Also contains some best practices for setting up a C++-Application: clang-format definitions, some sensible settings in CMakeLists.txt etc.

## Step 2: Add an external library & make it compile w/o actually using functionality.

- Add libmosquittopp to cmake & create a finder for it
- Link it to the application & try and call a member to see if it works.
- From here on we expect a MQTT-broker running on localhost on the default port (1883)

## Step 3: Connect to the local broker, send a simple message and quit.

- Create a local mosquittopp instance: auto mqtt = mosqpp::mosquittopp();
- use it to connect to the (hopefully running) broker on localhost and start the main loop
- Send a "Hello, World!" to the topic "/hello/mqtt"
- Disconnect and stop the loop.
- Clean up the library.

## Step 4: Read a temperature from a built-in sensor & write it to a topic cyclically

- Find out where to get the temperature and its format (Tip: https://www.kernel.org/doc/Documentation/thermal/sysfs-api.txt)
- Simple access: `/sys/class/thermal/thermal_zone*/temp`, for example:
  `$cat /sys/class/thermal/thermal_zone0/temp` yields `25000` (in millidegrees C) right now.
- `/sys/class/thermal/thermal_zone*/type` gives the type / data origin.
- Lets read the data from said file, re-format it to °C and print it to the `/temperature` topic.

## Step 5: Add a systemd service setup & create a debian installation package

- First, create the necessary files: the service definition and the postinst and prerm scripts
- Then, include CPack into CMake...
- ... and configure the package (/w dummy contents - we don't have a maintainer for example...)
- make package then creates a debian package that can be installed /w the usual board tools and contains the correct dependencies
