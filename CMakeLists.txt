PROJECT(mqttTemperature LANGUAGES CXX C)

CMAKE_MINIMUM_REQUIRED(VERSION 3.8.0)

SET(CMAKE_EXPORT_COMPILE_COMMANDS ON)

SET(CMAKE_CXX_STANDARD 17)

set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake ${CMAKE_MODULE_PATH})

find_package(Mosquittopp REQUIRED)

include_directories(${MOSQUITTOPP_INCLUDE_DIR})

set(SOURCES
    main.cpp
)

add_executable(mqttTemperature ${SOURCES})
target_link_libraries(mqttTemperature ${MOSQUITTOPP_LIBRARY})

SET(DESTDIR "/")
UNSET(CPACK_DEB_INSTALL_FILES)
INSTALL(TARGETS mqttTemperature DESTINATION /usr/bin)
INSTALL(FILES systemd/mqttTemperature.service DESTINATION /etc/systemd/system)

SET(CPACK_GENERATOR "DEB")
SET(CPACK_DEBIAN_PACKAGE_MAINTAINER "Mr. Tester")
SET(CPACK_DEBIAN_PACKAGE_SHLIBDEPS ON)

# optionally, add mosquitto as a dependency but it _actually_ isnt one since we might use another broker...
# SET(CPACK_DEBIAN_PACKAGE_DEPENDS "mosquitto (>= 1.6.9-1)")

SET(CPACK_DEBIAN_PACKAGE_CONTROL_EXTRA "${CMAKE_CURRENT_SOURCE_DIR}/systemd/prerm;${CMAKE_CURRENT_SOURCE_DIR}/systemd/postinst")

SET(CPACK_PACKAGE_VERSION 0.1)

INCLUDE(CPack)
